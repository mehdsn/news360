import re, sys, getopt

def attach_sign_chars(eq):
    """
    attach the sign character to the next term for easier processing
    
    example: 1 + 3x = - 2x^2  ==> 1 +3x = -2x^2
    """
    return eq.replace('+ ', '+').replace('- ', '-')

def swap_sign(term):
    """
    swaps the sign of a term in polynomial

    example: -3x ==> +3x, x^2 => -x^2
    """
    if term[0] == '-':
        return '+'+term[1:]
    elif term[0] == '+':
        return '-'+term[1:]
    else:
        return '-' + term

def move_to_left(eq):
    """
    moves the polynomial terms on the right side to the left and changes the sign 

    example: 1 +3x = -2x^2 ==> 1 +3x +2x^2
    """
    try:
        [left, right] = eq.split('=')
    except:
        raise 'Invalid Equation'
    return left.split() + [swap_sign(term) for term in right.split()]

def parse_term(term):
    """
    Parses a single polynomial term, returning the coefficient and (sorted) string representation of the variables

    example: '3x^2y' ==> [3, x^2y], '8yx^3' ==> [8, x^3y]
    """
    regex = "(^[+-]?[0-9.]*)(.*$)"
    m = re.search(regex, term)
    
    coef = m.group(1)
    if coef == '' or coef == '-' or coef == '+':
        coef += '1'
    coef = float(coef)
     
    _vars = m.group(2)
    vars_regex = "([a-zA-Z]\^?\d*)"
    variables = [match.group(1) for match in re.finditer(vars_regex, _vars)]

    variables.sort()
    
    return [coef, ''.join(variables)]

def print_function(coefs):
    """
    Given the coefficients and variables, this function returns the string representation of the equation
    """
    eq = ""
    for _vars in coefs:
        coef = coefs[_vars]
        if coef == 0:
            continue
        if coef > 0:
            eq += '' if len(eq) == 0 else '+ '
        else:
            eq += '-' if len(eq) == 0 else '- '
        if abs(coef) != 1:
            eq += str('{0:g}'.format(abs(coef)))
        
        eq += _vars + ' '
    return eq + '= 0'

def parse(eq):
    """
    Parses a polynomial equation and returns the canonical form of the euqation 

    example: x^2 + 3.5xy + y = y^2 - xy + y ==> 4.5xy + x^2 - y^2 = 0
    """
    # Removing extra spaces
    eq = ' '.join(eq.split())

    eq = attach_sign_chars(eq)
    left_eq = move_to_left(eq)
    if left_eq == None:
        return

    # Dictionary mapping variables to coefficients
    coefs = {}
    
    for term in left_eq:
        [coef, term] = parse_term(term)
        if term in coefs:
            coefs[term] += coef
        else:
            coefs[term] = coef

    return print_function(coefs)

def print_help():
    print """
        Usage: parser.py [-i <input file>]
        
        Options:
            -i < input file >

        Note: if -i option not provided, the program will run in interactive mode
        """
    
def main():
    input_file = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:")
    except getopt.GetoptError:
        print_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt == "-i":
            input_file = arg

    if input_file == None:
        while True:
            equation = raw_input('Please enter your equation: ')
            print parse(equation)
    else:
        fd_out = open(input_file + '.out', 'w')
        with open(input_file, 'r') as fd:
            for line in fd:
                fd_out.write(parse(line) + '\n')
        fd_out.close()
        fd.close()

if __name__ == "__main__":
    main()

